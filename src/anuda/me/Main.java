package anuda.me;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static String player1;              //Player 1 Name variable declaration
    static String player2;              //Player 2 Name variable declaration

    static Scanner scanner = new Scanner(System.in);       //Scanner object declaration to get inputs

    static ArrayList<ArrayList<String>> player1Ships = new ArrayList<ArrayList<String>>();      //player1 defense map array
    static ArrayList<ArrayList<String>> player2Ships = new ArrayList<ArrayList<String>>();      //player2 defense map array

    static ArrayList<ArrayList<String>> player1Attack = new ArrayList<ArrayList<String>>();     //player 1 offense map array
    static ArrayList<ArrayList<String>> player2Attack = new ArrayList<ArrayList<String>>();     //player2 offense map array

    static char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();    //array of letters from alphabet
    static int p1Points = 0;        //initializing player points
    static int p2Points = 0;
    static int win = 0;     //initializing win variable
    static String input;

    public static void main(String[] args) {
        System.out.println("\uD83D\uDEA2 Welcome to Battleship! Hit enter to start the game \uD83D\uDEA2");//Welcome Message
        scanner.nextLine();     //Delay for user to hit enter
        getPlayerNames();       //Call method to get player names
        printRules();       //print rules


        setupBlankBoardArrays();        //Setup of blank board arrays for players 1 and 2
        printBoard(1); //Print board of player 1
        setupShips(1, player1);     //call setup ships function for player 1
        clearScreen();          //clear screen before switching players
        printBoard(2);      //print board of player 2
        setupShips(2, player2);     //call setup ships function for player 2
        System.out.println("Ship Setup Complete... Hit Enter to Begin the Battle!");
        input = scanner.nextLine();     //begin the gameplay
        if(input.equals("r")){
            printRules();
        }        win = attack();     //set win to the result of the attack function
        if (win == 0) {
            System.out.println("Sorry, Unfortunately the program ran into an error...");    //return error if win is 0
        } else if (win == 1) {  //winner is player 1
            trophy(player1);       //call trophy function fro player 1
        } else {        //winner is player 2
            trophy(player2);        //call trophy function fro player 2
        }

    }

    private static void printRules(){
        System.out.println("Rules of the Game\n\nThe Ship Setup:\n\n- There are 5 ships (Carrier (5), Battleship(4), Cruiser(3), Submarine(3), Destroyer(2)). \n- In order to place the ships you will be asked the starting coordinate, and then the ending coordinate. \n- Make sure you fill the required spaces for each ship and that each ship is within the area of the board. \n- You can place the ship horizontally, or vertically, but not diagonally.\n\nThe Gameplay:\n\n- The aim of the game is to sink all of your opponent\\u2019s ships and obtain 17 points.\n- You will be asked which coordinate you would like to attack, if this coordinate corresponds with an opponent ship you will receive one point and a hit on your offense map. \n- Each player gets one move at a time\n- The first player to reach 17 points will be the winner\n\nOther Rules:\n\n- Use uppercase for the coordinate letter. Eg: A2, and not a2\n- Remember to hit enter before you pass to your opponent, so the screen will be cleared\n- If you want to see the rules at any point hit r - this will not work during the name setup.");
        System.out.println("Hit enter to continue");        //print rules and get enter input
        scanner.nextLine();
    }   //method to print rules

    private static void trophy(String player) {
        printBoard(1);      //print both players maps to show result
        printBoard(2);
        System.out.println("Congratulations " + player + ", you won!!! Hit Enter to claim your trophy");        //print congratulations message
        input = scanner.nextLine();
        if(input.equals("r")){
            printRules();
        }
        String trophyName = "";

            trophyName = player;    //trophyname is the player name
            for (int i = 0; i < (5 - player.length()); i++) {   //to fit properly in trophy, add blank space to the remaining space
                trophyName = trophyName + "_";
            }

        System.out.println(         //print trophy with player name
                "              .-=========-.\n" +
                        "              \\'-=======-'/\n" +
                        "              _|   .=.   |_\n" +
                        "             ((|  {{1}}  |))\n" +
                        "              \\|   /|\\   |/\n" +
                        "               \\__ '`' __/\n" +
                        "                 _`) (`_\n" +
                        "               _/_______\\_\n" +
                        "            _/____" + trophyName + "___\\_\n" +
                        "           /________________\\");


    }   //method to print trophy, requires player name

    private static int attack() {
        int hitResponse = 1;        //initialize hitresponse variables
        int hitResponse1 = 1;

        clearScreen();
        do {    //loop until player 1 points or player 2 points exceeds 16
            do {        //loop while hitresponse is 1
                System.out.println("Pass the computer to player 1, and hit enter to continue...");
                input = scanner.nextLine();
                if(input.equals("r")){
                    printRules();
                }
                clearScreen();      //clear the screen
                printBoard(1);      //print offense and defense boards to allow for decision making
                printBoard(3);
                if (hitResponse1 == 2) {
                    System.out.println("You were Hit!");        //print you were hit if the opponent was successful on his previous move
                }
                System.out.println(player1 + " Hits = " + p1Points + "      " + player2 + " Hits = " + p2Points);     //show player 1 and 2 points or hits
                String input1str;       //variable to store the user input
                do {
                    System.out.println(player1 + ", please enter the coordinate on your offense board you wish to attack. Eg: A2");
                    input1str = scanner.nextLine();
                    if(input1str.equals("r")){
                        printRules();
                    }
                } while (validateInputs(input1str) == null);   //keep getting user input until the input is valid
                Coordinate input1 = validateInputs(input1str);      //get the coordinate object by calling the validateinputs function and passing the input
                hitResponse = checkHitOrMiss(1, input1);       //pass player and input coordinate to checkhitormiss function and update hitresponse accordingly
                printBoard(1);      //print offense and defense boards
                printBoard(3);
                if (hitResponse == 1) {     //print result based on response from checkHitOrMiss function(1=already attacked, 2 = hit, 3 = miss)
                    System.out.println("You Have already attacked this coordinate, please try again...");
                } else if (hitResponse == 2) {
                    System.out.println("You HIT!");
                    if(p1Points==17){
                        return 1;
                    }
                } else {
                    System.out.println("You MISSED!");
                }
                System.out.println("Hit enter to continue...");
                input = scanner.nextLine();
                if(input.equals("r")){
                    printRules();
                }
                clearScreen();
            } while (hitResponse == 1);

            do {    //loop while hitresponse1 is 1
                System.out.println("Pass the computer to player 2, and hit enter to continue...");
                input = scanner.nextLine();
                if(input.equals("r")){
                    printRules();
                }
                clearScreen();      //clear the screen
                printBoard(2);      //print offense and defense boards to allow for decision making
                printBoard(4);
                if (hitResponse == 2) {
                    System.out.println("You were Hit!");            //print you were hit if the opponent was successful on his previous move
                }
                System.out.println(player1 + " Hits = " + p1Points + "      " + player2 + " Hits = " + p2Points);         //show player 1 and 2 points or hits
                String input2str;
                do {
                    System.out.println(player2 + ", please enter the coordinate on your offense board you wish to attack. Eg: A2");
                    input2str = scanner.nextLine();
                    if(input2str.equals("r")){
                        printRules();
                    }
                } while (validateInputs(input2str) == null);       //keep getting user input until the input is valid
                Coordinate input2 = validateInputs(input2str);      //get the coordinate object by calling the validateinputs function and passing the input
                hitResponse1 = checkHitOrMiss(2, input2);       //pass player and input coordinate to checkhitormiss function and update hitresponse accordingly
                printBoard(2);          //print offense and defense boards
                printBoard(4);
                if (hitResponse1 == 1) {       //print result based on response from checkHitOrMiss function(1=already attacked, 2 = hit, 3 = miss)
                    System.out.println("You Have already attacked this coordinate, please try again...");
                } else if (hitResponse1 == 2) {
                    System.out.println("You HIT!");
                    if(p2Points==171){
                        return 2;
                    }
                } else {
                    System.out.println("You MISSED!");
                }
                System.out.println("Hit enter to continue...");
                input = scanner.nextLine();
                if(input.equals("r")){
                    printRules();
                }                clearScreen();
            } while (hitResponse1 == 1);


        } while (p1Points < 17 || p2Points < 17);

        if (p1Points == 17) {
            return 1;   //player 1 won so return 1
        } else if (p2Points == 17) {
            return 2;  //player 2 won so return 2
        } else {
            return 0;       //code shouldn't reach this point, so 0 is an error
        }
    }       //method for the attack portion of the game

    private static int checkHitOrMiss(int player, Coordinate input) {

        if (player == 1) {  //check player to check the correct array
            if (player2Ships.get(input.getRow()).get(input.getColumn()).equals("__")) {     //if the coordinate is empty
                player2Ships.get(input.getRow()).set(input.getColumn(), "00");      //set the attack map and opponent defense map to Miss
                player1Attack.get(input.getRow()).set(input.getColumn(), "00");
                return 0;
            } else if (player2Ships.get(input.getRow()).get(input.getColumn()).equals("HH") || player2Ships.get(input.getRow()).get(input.getColumn()).equals("00")) {      //if the coordinate already has a hit or miss
                return 1;       //return 1 - coordinate has already been attacked
            } else {
                player2Ships.get(input.getRow()).set(input.getColumn(), "HH");      //set the attack map and opponent defense map to hit
                player1Attack.get(input.getRow()).set(input.getColumn(), "HH");
                p1Points = p1Points + 1;       //add a point to player 1 points
                return 2;       //return 2 - hit
            }
        } else {    //same as above but for player 2
            if (player1Ships.get(input.getRow()).get(input.getColumn()).equals("__")) {
                player1Ships.get(input.getRow()).set(input.getColumn(), "00");
                player2Attack.get(input.getRow()).set(input.getColumn(), "00");

                return 0;
            } else if (player1Ships.get(input.getRow()).get(input.getColumn()).equals("HH") || player1Ships.get(input.getRow()).get(input.getColumn()).equals("00")) {
                return 1;
            } else {
                player1Ships.get(input.getRow()).set(input.getColumn(), "HH");
                player2Attack.get(input.getRow()).set(input.getColumn(), "HH");
                p2Points = p2Points + 1;

                return 2;
            }
        }

    }   //method to check if hit or miss, requires player code and input coordinate object

    private static void setupShips(int player, String playerName) {

        System.out.println("Let's begin the ship setup for " + playerName + ". Make sure only you can see the screen. Hit enter to continue...");   //Make sure only player can see screen
        input = scanner.nextLine();
        if(input.equals("r")){
            printRules();
        }
        String setupResponse;      //initialize response for setup

        //loop setup for each ship until setup response = successfully placed
        do {
            setupResponse = setupAShip("Carrier", 5, "CA", player);     //call the setup a ship method and pass ship name, number of spaces and player name
            printBoard(player);     //Show board to player
            System.out.println(setupResponse);      //print the response
        } while (!setupResponse.equals("Successfully placed the Carrier"));
        do {
            setupResponse = setupAShip("Battleship", 4, "BA", player);
            printBoard(player);
            System.out.println(setupResponse);
        } while (!setupResponse.equals("Successfully placed the Battleship"));
        do {
            setupResponse = setupAShip("Cruiser", 3, "CR", player);
            printBoard(player);
            System.out.println(setupResponse);
        } while (!setupResponse.equals("Successfully placed the Cruiser"));
        do {
            setupResponse = setupAShip("Submarine", 3, "SU", player);
            printBoard(player);
            System.out.println(setupResponse);
        } while (!setupResponse.equals("Successfully placed the Submarine"));
        do {
            setupResponse = setupAShip("Destroyer", 2, "DE", player);
            printBoard(player);
            System.out.println(setupResponse);
        } while (!setupResponse.equals("Successfully placed the Destroyer"));
    }       //method to setup ships, requires player code and name

    private static String setupAShip(String shipName, int spaces, String shortName, int player) {

        String start;   //initialize var for user input for start coordinate
        Coordinate startCoordinate;     //start coordinate object with row and column number
        String end;     //initialize var for user input for end coordinate
        Coordinate endCoordinate;       //end coordinate object with row and column number
        do {
            System.out.println("Where do you want to place your " + shipName + "-" + spaces + " spaces. Input the starting coordinate. Eg: A2");
            start = scanner.nextLine();     //get start coordinate
            if(start.equals("r")){
                printRules();
            }
        } while (validateInputs(start) == null);        //keep repeating if the until the input is valid
        startCoordinate = validateInputs(start);        //set start coordinate
        do {
            System.out.println("Input the ending coordinate for your " + shipName + "-" + spaces + " spaces. You entered the start coordinate as " + start);
            end = scanner.nextLine();       //get end coordinate
            if(end.equals("r")){
                printRules();
            }
        } while (validateInputs(end) == null);      //keep repeating if the until the input is valid
        endCoordinate = validateInputs(end);        //set end coordinate

        System.out.println("You want to place your " + shipName + "-" + spaces + ". Between " + start + " and " + end + ". Is this correct? Hit y for yes, and any other key for no");
        input = scanner.nextLine();
        if(input.equals("r")){
            printRules();
        }
        if (!input.equals("y")) {      //verify user input
            return "Please Try again...";
        }

        if (endCoordinate.getColumn() - startCoordinate.getColumn() + 1 == spaces || endCoordinate.getRow() - startCoordinate.getRow() + 1 == spaces) {     //check if the input occupy all spaces

            return updateBoardShips(startCoordinate, endCoordinate, startCoordinate.getRow(), startCoordinate.getColumn(), spaces, shortName, player, shipName);    //pass the data to updateboardships method to update the map arrays


        } else if (startCoordinate.getColumn() - endCoordinate.getColumn() + 1 == spaces || startCoordinate.getRow() - endCoordinate.getRow() + 1 == spaces) {  //error handling if the start coordinate>end coordinate
            return updateBoardShips(startCoordinate, endCoordinate, endCoordinate.getRow(), endCoordinate.getColumn(), spaces, shortName, player, shipName);

        } else {

            return "The ship has to take up " + spaces + " spaces within the area of the board. Please Try Again";      //if doesn't occupy the required spaces return error
        }
    }   //method to setup a ship, requires ship name, spaces, short name, and player code

    private static String updateBoardShips(Coordinate startCoordinate, Coordinate endCoordinate, int row, int column, int spaces, String shortName, int player, String shipName) {

        if (startCoordinate.getColumn() == endCoordinate.getColumn()) { //check if ship is set horizontally

            for (int i = 0; i < spaces; i++) {      //loop through number of spaces and check if positions are empty
                if (player == 1) {      //check if a ship has already been placed and return error if so
                    if (!player1Ships.get(row + i).get(column).equals("__")) {
                        return "A ship has already been placed between the spaces you have selected";
                    }
                } else {
                    if (!player2Ships.get(row + i).get(column).equals("__")) {
                        return "A ship has already been placed between the spaces you have selected";
                    }
                }
            }
            for (int i = 0; i < spaces; i++) {      //loop through and update the arrays based on player
                if (player == 1) {
                    player1Ships.get(row + i).set(column, shortName);
                } else {
                    player2Ships.get(row + i).set(column, shortName);
                }
            }
            return "Successfully placed the " + shipName;       //return success message
        } else if (startCoordinate.getRow() == endCoordinate.getRow()) {    //check if ship is set vertically
            for (int i = 0; i < spaces; i++) {      //loop through number of spaces and check if positions are empty
                if (player == 1) {      //check if a ship has already been placed and return error if so
                    if (!player1Ships.get(row).get(column + i).equals("__")) {
                        return "A ship has already been placed between the spaces you have selected";
                    }
                } else {
                    if (!player2Ships.get(row).get(column + i).equals("__")) {
                        return "A ship has already been placed between the spaces you have selected";
                    }
                }
            }
            for (int i = 0; i < spaces; i++) {      //loop through and update the arrays based on player
                if (player == 1) {
                    player1Ships.get(row).set(column + i, shortName);
                } else {
                    player2Ships.get(row).set(column + i, shortName);
                }
            }
            return "Successfully placed the " + shipName;       //return success message
        } else {
            return "You cannot place ships diagonally. Please Try Again...";    //ship is placed diagonally so return error
        }
    }   //method to update arrays with ships, requires start and end coordinate objects, row, column, spaces, shortname, player code, and ship name

    private static Coordinate validateInputs(String coordinate) {

        try {
            Integer column = Character.getNumericValue(coordinate.charAt(1)) - 1;   //get second char from input and adjust because arrays start at 0
            char rowInput = coordinate.charAt(0);           //get the first chart from input as the row number

            Integer row = 8;


            for (int i = 0; i < alphabet.length; i++) { //cycle through letters in alphabet array and set row to the index based on the input
                if (rowInput == alphabet[i]) {
                    row = i;
                }
            }

            if (row < 8 && column < 8 && column >= 0) { //check if row and column is within the limits - 8 rows, columns
                Coordinate result = new Coordinate();   //set new coordinate object, set row and column and return
                result.setColumn(column);
                result.setRow(row);
                return result;

            } else {
                System.out.println("Invalid input! Please enter the coordinate as row, column. Example: C3");
                return null;    //return null because not within limits
            }

        } catch (Exception e) {     //catch any input format exceptions(i.e. abnormal data) and return error
            System.out.println("Invalid input! Please enter the coordinate as row, column. Example: C3");
            return null;
        }


    }       //validate inputs and return coordinate object, requires string input

    private static void printBoard(Integer map) {


        if (map == 1) {  //Print Map name based on the map code - 1 = player 1 defense, 2 = player 2 defense, 3 = player 1 offense and 4 = player 2 offense
            System.out.println(player1 + "'s Defense Map");
        } else if (map == 2) {
            System.out.println(player2 + "'s Defense Map");
        } else if (map == 3) {
            System.out.println(player1 + "'s Offense Map");
        } else {
            System.out.println(player2 + "'s Offense Map");
        }

        System.out.println("    1  2  3  4  5  6  7  8");   //printing the column index

        for (int i = 0; i < 8; i++) {       //Loop through the 8 rows
            StringBuilder row = new StringBuilder();
            row.append(" " + alphabet[i] + " ");    //get the row index letter from the alphabet array and append to the row string

            for (int j = 0; j < 8; j++) {       //loop through 8 columns, get data for each position and append to the row string.
                if (map == 1) {          //Decide which array to take the data from based on map
                    row.append(player1Ships.get(i).get(j)).append(" ");
                } else if (map == 2) {
                    row.append(player2Ships.get(i).get(j)).append(" ");
                } else if (map == 3) {
                    row.append(player1Attack.get(i).get(j)).append(" ");

                } else {
                    row.append(player2Attack.get(i).get(j)).append(" ");

                }
            }
            System.out.println(row + "\n"); //print the row
        }

    }     //map to print method, requires map code

    private static void setupBlankBoardArrays() {

        //Declaring the arrays for Horizontal rows A-H for player 1 defense board
        ArrayList<String> a1 = new ArrayList<String>();
        ArrayList<String> b1 = new ArrayList<String>();
        ArrayList<String> c1 = new ArrayList<String>();
        ArrayList<String> d1 = new ArrayList<String>();
        ArrayList<String> e1 = new ArrayList<String>();
        ArrayList<String> f1 = new ArrayList<String>();
        ArrayList<String> g1 = new ArrayList<String>();
        ArrayList<String> h1 = new ArrayList<String>();

        //Declaring the arrays for Horizontal rows A-H for player 2 defense board
        ArrayList<String> a2 = new ArrayList<String>();
        ArrayList<String> b2 = new ArrayList<String>();
        ArrayList<String> c2 = new ArrayList<String>();
        ArrayList<String> d2 = new ArrayList<String>();
        ArrayList<String> e2 = new ArrayList<String>();
        ArrayList<String> f2 = new ArrayList<String>();
        ArrayList<String> g2 = new ArrayList<String>();
        ArrayList<String> h2 = new ArrayList<String>();

        //Declaring the arrays for Horizontal rows A-H for player 1 offense board
        ArrayList<String> a3 = new ArrayList<String>();
        ArrayList<String> b3 = new ArrayList<String>();
        ArrayList<String> c3 = new ArrayList<String>();
        ArrayList<String> d3 = new ArrayList<String>();
        ArrayList<String> e3 = new ArrayList<String>();
        ArrayList<String> f3 = new ArrayList<String>();
        ArrayList<String> g3 = new ArrayList<String>();
        ArrayList<String> h3 = new ArrayList<String>();

        //Declaring the arrays for Horizontal rows A-H for player 2 offense board
        ArrayList<String> a4 = new ArrayList<String>();
        ArrayList<String> b4 = new ArrayList<String>();
        ArrayList<String> c4 = new ArrayList<String>();
        ArrayList<String> d4 = new ArrayList<String>();
        ArrayList<String> e4 = new ArrayList<String>();
        ArrayList<String> f4 = new ArrayList<String>();
        ArrayList<String> g4 = new ArrayList<String>();
        ArrayList<String> h4 = new ArrayList<String>();

        //Adding the default __ to all values in these rows

        for (int i = 0; i < 8; i++) {
            a1.add("__");
            b1.add("__");
            c1.add("__");
            d1.add("__");
            e1.add("__");
            f1.add("__");
            g1.add("__");
            h1.add("__");

            a2.add("__");
            b2.add("__");
            c2.add("__");
            d2.add("__");
            e2.add("__");
            f2.add("__");
            g2.add("__");
            h2.add("__");

            a3.add("__");
            b3.add("__");
            c3.add("__");
            d3.add("__");
            e3.add("__");
            f3.add("__");
            g3.add("__");
            h3.add("__");

            a4.add("__");
            b4.add("__");
            c4.add("__");
            d4.add("__");
            e4.add("__");
            f4.add("__");
            g4.add("__");
            h4.add("__");
        }

        //Adding these arrays to another to create a two dimensional array for player 1 defense
        player1Ships.add(a1);
        player1Ships.add(b1);
        player1Ships.add(c1);
        player1Ships.add(d1);
        player1Ships.add(e1);
        player1Ships.add(f1);
        player1Ships.add(g1);
        player1Ships.add(h1);

        //Adding these arrays to another to create a two dimensional array for player 2 defense
        player2Ships.add(a2);
        player2Ships.add(b2);
        player2Ships.add(c2);
        player2Ships.add(d2);
        player2Ships.add(e2);
        player2Ships.add(f2);
        player2Ships.add(g2);
        player2Ships.add(h2);

        //Adding these arrays to another to create a two dimensional array for player 1 offense
        player1Attack.add(a3);
        player1Attack.add(b3);
        player1Attack.add(c3);
        player1Attack.add(d3);
        player1Attack.add(e3);
        player1Attack.add(f3);
        player1Attack.add(g3);
        player1Attack.add(h3);

        //Adding these arrays to another to create a two dimensional array for player 2 offense
        player2Attack.add(a4);
        player2Attack.add(b4);
        player2Attack.add(c4);
        player2Attack.add(d4);
        player2Attack.add(e4);
        player2Attack.add(f4);
        player2Attack.add(g4);
        player2Attack.add(h4);
    }   //method to initialize the blank map arrays

    private static void getPlayerNames() {           //Method to get player names
        for (int i = 1; i < 3; i++) {               //For loop to get the player names
            System.out.println("What is your name, player " + i);   //Prompt to get input
            if (i == 1) {       //conditional statement to see which variable to assign to
                player1 = scanner.nextLine();
            } else {
                player2 = scanner.nextLine();
            }
        }
        System.out.println("Is this correct? (Hit y if yes and any other key if no)\nPlayer 1: " + player1 + "\nPlayer 2: " + player2);     //Validate player names

        input=scanner.nextLine();
        if(input.equals("r")){
            printRules();
        }
        if (!input.equals("y")) {       //Call function again if user doesn't input y
            System.out.println("Please try again");
            getPlayerNames();
        }
    }   //method to get player names

    private static void clearScreen() {
        for (int i = 0; i < 1000; i++) {      //loop through and add a bunch of empty lines
            System.out.println("\n");
        }
    }       //method to clear screen

}
